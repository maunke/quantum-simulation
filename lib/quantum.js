/* 
The MIT License (MIT)

Copyright (c) 2016 Markus Unkel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
// =========================
// QUANTUM 1D PHYSICS ENGINE
// =========================
/* PIXI CONTAINER CHILD INDICES
 * 2: Physics
    * 0: Potential / behind potential E line of wavefunction
    * 1: x/k Wavefunction
    * 2: <- 1.1 y Label
 * 1: Labels
    * 0: xk Label
    * "1": y Label (+ in case of ZeroCN potential borders at 0 and _plotting.height)
 * 0: Grid
*/
function Quantum1D (){
  return this;
}
// =============
// y Pos Convert
// =============
Quantum1D.prototype.yConvert = function (y) {
  return - y + this._plotting.height;
};
// =============
// x Pos Convert
// =============
Quantum1D.prototype.xToPx = function (x) {
  var grid = this._config.grid;
  var width = grid.xmax - grid.xmin;
  var px = (x - grid.xmin) / width * this._plotting.width; 
  return px;
};
// ============================
// this._plotting Style Options
// ============================
Quantum1D.prototype.Plot = function () {
  var width = this._plotting.width;
  var config = this._config;
  var density = config.density;
  var sampling = config.length/width/density;
  var factor = config.wavefactor;
  var wave = this._wavepacket;
  var color = config.wavefunction.color;
  var waveGeo = this._plotting._scene.getChildAt(2).getChildAt(1);
  var heightOffset = this._plotting._scene.getChildAt(1).getChildAt(0).height || 0;
  waveGeo.clear();
  var colorWave = (color.E) ? color.xE : color.x0;
  waveGeo.beginFill(colorWave);
  waveGeo.moveTo(0,this.yConvert(heightOffset+config.wavefactor));
  for ( var i = 0; i < density*width; i++) {
    waveGeo.lineTo(i/density, this.yConvert(+config.wavescale*wave[sampling*i].abs2()+heightOffset+config.wavefactor));
  }
  waveGeo.lineTo(width, this.yConvert(heightOffset+config.wavefactor));
  waveGeo.lineTo(0, this.yConvert(heightOffset+config.wavefactor));
  waveGeo.endFill();
  // case momentum
  if (config.momentum) {
    var factor = config.momentumfactor;
    FFT(wave,false);
    waveGeo.beginFill(color.k);
    waveGeo.moveTo(0,this.yConvert(heightOffset));
    for ( var i = 0; i < density*width; i++) {
      waveGeo.lineTo(width-i/density, this.yConvert(factor*wave[sampling*i].abs2()+heightOffset));
    }
    waveGeo.lineTo(width, this.yConvert(heightOffset));
    waveGeo.lineTo(0, this.yConvert(heightOffset));
    waveGeo.endFill();
    FFT(wave,true);
  }
};

Quantum1D.prototype.expTPValues = function () {
  // expT = -k^2*dt*h/(2*m)
  var expT = new Array();
  var length = this._wavepacket.length;
  var k = this._grid.k;
  var config = this._config;
  var dt = config.dt;
  var mass = config.wavefunction.mass; 
  var factor = H_RED/(2*mass);
  for (var i = 0; i < length; i++) {
    var t = new Complex();
    t.re = 0;
    t.im = -k[i]*k[i]*dt*factor; 
    t.cexp(t);
    expT.push(t);
  }
  this._expT = expT;
  // expV = - V/h*dt/2
  var expV = new Array();
  for (var i = 0; i < length; i++) {
    var p = new Complex();
    p.re = 0;
    p.im = -this._potential[i]/H_RED*dt/2;
    p.cexp(p);
    expV.push(p);
  }
  this._expV = expV;
};
// ============
// T Propagator
// ============
Quantum1D.prototype.TPropagator = function () {
  var wave = this._wavepacket;
  var length = wave.length;
  var expT = this._expT;
  for ( var i = 0; i < length; i++) {
    wave[i].mul(expT[i],wave[i]);
    }
};
// ============
// V Propagator
// ============
Quantum1D.prototype.VPropagator = function () {
  var wave = this._wavepacket;
  var length = wave.length;
  var expV = this._expV;
  for ( var i = 0; i < length; i++) {
    wave[i].mul(expV[i],wave[i]);
    }
};
// ================
// Create Potential
// ================
Quantum1D.prototype.CreatePotential = function (potConfig) {
  var self = this;
  // Potential types
  // ===============
  // harmonic oscillator v(x) = m/2*w^2*x^2
  // input w, x0, xmin, xmax
  var osci = function (x) {
    var w = potConfig.w;
    var m = self._config.wavefunction.mass;
    var x0 = potConfig.x0;
    var xMin = potConfig.xMin;
    var xMax = potConfig.xMax;
    var pot = 0.0;
    if (x > xMin && x < xMax)
      pot = m / 2 * w * w * (x - x0) * (x - x0);
    return pot;
  };
  // rectangular 
  // config input: energy, width, x0 (center of potential)
  var rect = function (x) {
    var factor = 1600/(self._config.grid.xmax - self._config.grid.xmin);
    var width = potConfig.width;
    var xmin = potConfig.x0 - width / 2;
    var xmax = potConfig.x0 + width / 2;
    var energy = potConfig.energy;
    var alpha = potConfig.alpha;
    var pot = energy * (1 / ( 1 + Math.exp(-(x-xmin)/alpha))-1 / ( 1 + Math.exp(-(x-xmax)/alpha)));
    return pot;
  };
  // gauss 
  // config input: energy, x0 (center of potential), sigma
  var gauss = function (x) {
    var sigma = potConfig.sigma;
    var x0 = potConfig.x0;
    var energy = potConfig.energy;
    var pot = energy*Math.exp(-(x-x0)*(x-x0)/(2*sigma*sigma));
    return pot;
  };
  // personal 
  // config input: energy, x0 (center of potential), sigma
  var personal = function (x) {
    var xmin = potConfig.xMin;
    var xmax = potConfig.xMax;
    var func = eval(potConfig.potFunc);
    if (x >= xmin && x <= xmax)
      return func;
    else
      return 0;
    /*
    var pot = energy*Math.exp(-(x-x0)*(x-x0)/(2*sigma*sigma));
    return pot;
    */
  };
  // potential created over x 
  var fillPotential = function (callback) {
    var potential = self._potential;
    for ( var i = 0; i < self._config.length; i++)
      potential[i] += callback(self._grid.x[i]);
  };
  
  function typeCall (t) {
    switch(t) {
      case 'Rect':
        fillPotential(rect);
        break;
      case 'Osci':
        fillPotential(osci);
        break;
      case 'Gauss':
        fillPotential(gauss);
        break;
      case 'Personal':
        fillPotential(personal);
        break;
      default:
        alert("False potential type!");
    } 
  };
  typeCall(potConfig.type);
};
// ========================
// Initial State of Physics
// ========================
Quantum1D.prototype.InitialStatePhysics = function (fourier) {
  var length = this._config.length;
  BitReversal(length);
  // init grid
  this._grid = {};
  var config = this._config;
  var grid = this._grid;
  grid.x = new Array(config.length);
  grid.k = new Array(config.length);
  grid.dx = (config.grid.xmax-config.grid.xmin)/(config.length-1);
  for ( var i = 0; i < config.length/2; i++) {
    grid.x[config.length/2+i] = (0.5+i)*grid.dx;
    grid.x[config.length/2-1-i] = -(0.5+i)*grid.dx;
  }

  var kmin = - Math.PI / grid.dx;
  grid.dk = 2*Math.PI / (grid.dx * (config.length-1));
  for ( var i = 0; i < config.length; i++) {
    grid.k[config.length/2+i] = (0.5+i)*grid.dk;
    grid.k[config.length/2-1-i] = -(0.5+i)*grid.dk;
  }

  // create potential 
  // ================
  var potential = new Array();
  for ( var i = 0; i < length; i++)
    potential.push(0.0);
  this._potential = potential;
  for (var l = 0; l < config.potential.length; l++) {
    var potConfig = config.potential[l];
    this.CreatePotential(potConfig);
  }
  // create wavefunction 
  // ===================
  var wave = new Array();
  var factor = new Complex();
  var gaussian = new Complex();
  var waveConfig = this._config.wavefunction;
  var sigma = waveConfig.sigma;
  var energy = waveConfig.energy;
  var mass = waveConfig.mass;
  var x0 = waveConfig.x0;
  var k0 = Math.sqrt(energy*2*mass/(H_RED*H_RED)-1/(2*sigma*sigma));

  for ( var i = 0; i < length; i++) {
    if (fourier)
      factor.re = Math.pow(-1,i);
    else
      factor.re = 1;
    factor.im = 0.0;
    gaussian = GaussianWave(grid.x[i],x0,sigma,k0);
    wave.push(gaussian.mul(factor,gaussian));
  }
  this._wavepacket = wave;
};
// ==================
// Initial Containers
// ==================
Quantum1D.prototype.InitialContainers = function () {
  this._plotting._scene = new PIXI.Container();
  scene = this._plotting._scene;
  // grid
  scene.addChildAt(new PIXI.Container(), 0);
  // labels
  scene.addChildAt(new PIXI.Container(), 1);
  for (var j = 0; j < 1; j++) 
    scene.getChildAt(1).addChildAt(new PIXI.Container(), j);
  
  // physics
  scene.addChildAt(new PIXI.Container(), 2);
};
// ============
// Physics Plot
// ============
Quantum1D.prototype.PhysicsPlot = function () {
  // calculate wave- & potfactor for optimal displaying
  // idea: greatest potential height project to goldenratio plot height = 0.618 * (plotting.height-xLabel.height)
  // momentum plot to 0.382 plot height
  var config = this._config;
  var physicsHeight = this._plotting.height - this._plotting._scene.getChildAt(1).getChildAt(0).height;
  var maxEnergy = Math.max(...[Math.abs(Math.max(...this._potential)),Math.abs(Math.min(...this._potential)),config.wavefunction.energy]);
  if (config.maxEnergy) {
    if (config.maxEnergy != maxEnergy) {
      maxEnergy = config.maxEnergy;
    }
  }
  config.maxEnergy = maxEnergy;
  config.physicsHeight = physicsHeight;
  // color E > 0
  config.wavefunction.color.E = true;
  var waveFactor = config.wavefunction.energy;
  config.potfactor = (maxEnergy>0) ? 0.618 * physicsHeight / maxEnergy : 1;
  config.wavefactor = (maxEnergy>0) ? 0.618 * physicsHeight / maxEnergy * waveFactor : 0;
  config.wavescale = 0.25 * physicsHeight;
  // calculate start momentum factor
  var momentum = FFT(this._wavepacket,false);
  var momabs2 = new Array(config.length);
  for ( var i = 0; i < config.length; i++)
    momabs2[i] = momentum[i].abs2();
  var maxMomentum = Math.max(...momabs2);
  config.momentumfactor = 0.382 * physicsHeight / maxMomentum; 
  FFT(this._wavepacket,true);
  var heightOffset = this._plotting._scene.getChildAt(1).getChildAt(0).height || 0;
  var physics = this._plotting._scene.getChildAt(2);
  var width = this._plotting.width;
  var density = config.density;
  var sampling = config.length/width/density;
  var potGeo = new PIXI.Graphics();
  // draw grid if grid is true
  function PlotGrid(plotter) {
    if (plotter._design.grid.plot) {
      var physicsHeight = config.physicsHeight;
      // draw x vertical grid line
      var grid = plotter._config.grid;
      var xMin = grid.xmin;
      var xMax = grid.xmax;
      var design = plotter._design.xLabel;
      var delta = design.delta;
      var xGridColor = plotter._design.grid.color;
      var xValue = xMin + delta;
      while(xValue < xMax) {
        potGeo.beginFill(xGridColor);
        potGeo.moveTo(plotter.xToPx(xValue),plotter.yConvert(heightOffset));
        potGeo.lineTo(plotter.xToPx(xValue), plotter.yConvert(heightOffset+physicsHeight));
        potGeo.lineTo(plotter.xToPx(xValue)+1, plotter.yConvert(heightOffset+physicsHeight));
        potGeo.lineTo(plotter.xToPx(xValue)+1, plotter.yConvert(heightOffset));
        potGeo.lineTo(plotter.xToPx(xValue), plotter.yConvert(heightOffset));
        potGeo.endFill();   
        xValue += delta;
      }
      // draw y horizontal grid line
      var design = plotter._design.yLabel;
      var maxEnergy = config.maxEnergy;
      var sampling = design.sampling;
      var height = plotter._plotting.height;
      var deltaEnergy = maxEnergy / (sampling);
      var deltaHeight = physicsHeight * 0.618 / (sampling);  
      var fontsize = design.fontsize;
      var colorLabel = design.color.label;
      var plotHeight = height - physicsHeight + deltaHeight;
      var energy = deltaEnergy;
      var x0 = 0;
      var x1 = plotter._plotting.width;
      var plotOverMax = true;
      while (plotHeight < physicsHeight && plotOverMax) {
        // verts for energy labels
        potGeo.beginFill(xGridColor);
        var y0 = plotter.yConvert(plotHeight - 1);
        var y1 = plotter.yConvert(plotHeight);
        potGeo.moveTo(x0,y0);
        potGeo.lineTo(x0,y1);
        potGeo.lineTo(x1,y1);
        potGeo.lineTo(x1,y0);
        potGeo.lineTo(x0,y0);
        potGeo.endFill();
        plotHeight += deltaHeight;
        energy += deltaEnergy;
        if (energy > maxEnergy*1.5)
          plotOverMax = false;
      }
    }
  }
  PlotGrid(this);
  // draw Energy Level of Wavefunction
  potGeo.beginFill(config.wavefunction.color.line);
  potGeo.moveTo(0,this.yConvert(heightOffset+config.wavefactor));
  potGeo.lineTo(width, this.yConvert(heightOffset+config.wavefactor));
  potGeo.lineTo(width, this.yConvert(heightOffset+config.wavefactor-1));
  potGeo.lineTo(0, this.yConvert(heightOffset+config.wavefactor-1));
  potGeo.lineTo(0, this.yConvert(heightOffset+config.wavefactor));
  potGeo.endFill();
  // draw potential
  potGeo.beginFill(config.potColor.pos);
  potGeo.moveTo(0,this.yConvert(heightOffset));
  for ( var i = 0; i < density*width; i++) {
    var potValue = this._potential[sampling*i];
    potValue = (potValue >= 0) ? potValue : 0;
    // color for negative or positibe potential
    // define vertices
    potGeo.lineTo(i/density, this.yConvert(this._config.potfactor*potValue+heightOffset));
  }
  potGeo.lineTo(width, this.yConvert(this._config.potfactor*Math.abs(this._potential[this._potential.length-1])+heightOffset));
  potGeo.lineTo(width, this.yConvert(heightOffset));
  potGeo.lineTo(0, this.yConvert(heightOffset));
  potGeo.endFill();
  potGeo.beginFill(config.potColor.neg);
  potGeo.moveTo(0,this.yConvert(heightOffset));
  for ( var i = 0; i < density*width; i++) {
    var potValue = this._potential[sampling*i];
    potValue = (potValue < 0) ? Math.abs(potValue) : 0;
    // color for negative or positibe potential
    // define vertices
    potGeo.lineTo(i/density, this.yConvert(this._config.potfactor*potValue+heightOffset));
  }
  potGeo.lineTo(width, this.yConvert(this._config.potfactor*Math.abs(this._potential[this._potential.length-1])+heightOffset));
  potGeo.lineTo(width, this.yConvert(heightOffset));
  potGeo.lineTo(0, this.yConvert(heightOffset));
  potGeo.endFill();
  physics.addChildAt(potGeo, 0);

  var wave = this._wavepacket;
  var waveGeo = new PIXI.Graphics();
  var color = config.wavefunction.color;
  var colorWave = (color.E) ? color.xE : color.x0;
  waveGeo.beginFill(colorWave);
  waveGeo.moveTo(0,this.yConvert(heightOffset+config.wavefactor));
  for ( var i = 0; i < density*width; i++) {
    waveGeo.lineTo(i/density, this.yConvert(config.wavescale*wave[sampling*i].abs2()+heightOffset+config.wavefactor));
  }
  waveGeo.lineTo(width, this.yConvert(heightOffset+config.wavefactor));
  waveGeo.lineTo(0, this.yConvert(heightOffset+config.wavefactor));
  waveGeo.endFill();
  if (config.momentum) {
    var factor = config.momentumfactor;
    FFT(wave,false);
    waveGeo.beginFill(color.k);
    waveGeo.moveTo(0,this.yConvert(heightOffset));
    for ( var i = 0; i < density*width; i++) {
      waveGeo.lineTo(width-i/density, this.yConvert(factor*wave[sampling*i].abs2()+heightOffset));
    }
    waveGeo.lineTo(width, this.yConvert(heightOffset));
    waveGeo.lineTo(0, this.yConvert(heightOffset));
    waveGeo.endFill();
    FFT(wave,true);
  }
  physics.addChildAt(waveGeo, 1);

};
/*
// ================
// Thomas Algorithm
// ================
Quantum1D.prototype.ThomasAlgorithm = function () {
  var config = this._config;
  var length = config.length;
  var potential = this._potential;
  var mass = config.wavefunction.mass;
  var dx = this._grid.dx;
  var dt = config.dt;
  var wave = this._wavepacket;
  // create omega
  // ============
  var omega = new Array();
    var factor = new Complex();
    factor.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[0];
    factor.im = 4 * mass / H_RED * dx * dx / dt;
  // i = 0
  var value = new Complex();
  value.add(wave[0],value);
  value.mul(factor,value);
  value.sub(wave[length-1],value);
  value.sub(wave[1],value);
  omega.push(value);
  for (var i = 1; i < length-1; i++) {
    var factor = new Complex();
    factor.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[i];
    factor.im = 4 * mass / H_RED * dx * dx / dt;
    var value = new Complex();
    value.add(wave[i],value);
    value.mul(factor,value);
    value.sub(wave[i-1],value);
    value.sub(wave[i+1],value);
    omega.push(value);
  }
  // i = length - 1
  var factor = new Complex();
  factor.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[length-1];
  factor.im = 4 * mass / H_RED * dx * dx / dt;
  var value = new Complex();
  value.add(wave[length-1],value);
  value.mul(factor,value);
  value.sub(wave[length-2],value);
  value.sub(wave[0],value);
  omega.push(value);
  // get cPrime & b
  // ===========
  var cPrime = this._matrix.cPrime;
  var b = this._matrix.b;
  // create dPrime
  // =============
  var dPrime = new Array();
  // i = 0
  var value = new Complex();
  value.add(omega[0],value);
  value.div(b[0],value);
  dPrime.push(value);
  for (var i = 1; i < length-1; i++) {
    var value = new Complex();
    var value2 = new Complex();
    value.add(omega[i],value);
    value.sub(dPrime[i-1],value);
    value2.add(b[i],value2);
    value2.sub(cPrime[i-1],value2);
    value.div(value2,value);
    dPrime.push(value);
  }
  // get chi1
  var chi1 = new Array(length-1);
  chi1[length-2] = new Complex();
  chi1[length-2].add(dPrime[length-2],chi1[length-2]);
  for (var i = length - 3; i >= 0; i--){
    var factor = new Complex();
    chi1[i] = new Complex();
    chi1[i].add(dPrime[i],chi1[i]);
    factor.add(cPrime[i],factor);
    factor.mul(chi1[i+1],factor);
    chi1[i].sub(factor,chi1[i]); 
  }
  // overwrite dPrime with second equation
  var value = new Complex(-1.0,0);
  value.div(b[0],value);
  dPrime[0] = value;
  for (var i = 1; i < length - 2; i++) {
    var value = new Complex();
    value.add(b[i],value);
    value.sub(cPrime[i-1],value);
    dPrime[i] = new Complex();
    dPrime[i].sub(dPrime[i-1],dPrime[i]);
    dPrime[i].div(value,dPrime[i]);
  }
  var value = new Complex(0,0);
  value.add(b[length-2],value);
  value.sub(cPrime[length-3],value);
  dPrime[length-2] = new Complex(-1.0,0);
  dPrime[length-2].sub(dPrime[length-3],dPrime[length-2]);
  dPrime[length-2].div(value,dPrime[length-2]);
  // get chi2
  var chi2 = new Array(length-1);
  chi2[length-2] = new Complex();
  chi2[length-2].add(dPrime[length-2],chi2[length-2]);
  for (var i = length - 3; i >= 0; i--){
    var factor = new Complex();
    chi2[i] = new Complex();
    chi2[i].add(dPrime[i],chi2[i]);
    factor.add(cPrime[i],factor);
    factor.mul(chi2[i+1],factor);
    chi2[i].sub(factor,chi2[i]); 
  }
  // get wave 
  var wave = this._wavepacket;
  // compute psi_n
  var value1 = new Complex();
  value1.add(omega[length-1],value1);
  value1.sub(chi1[0],value1);
  value1.sub(chi1[length-2],value1);
  var value2 = new Complex();
  value2.add(b[length-1],value2);
  value2.add(chi2[0],value2);
  value2.add(chi2[length-2],value2);
  wave[length-1] = value1.div(value2,value1);
  // compute psi_i
  for (var i = 0; i < length - 1; i++) {
    var factor = new Complex();
    factor.add(chi2[i],factor);
    factor.mul(wave[length-1],factor);
    factor.add(chi1[i],factor);
    wave[i] = factor;
  }
};
// ================================
// CN Coefficients a, b_j, c, omega
// ================================
Quantum1D.prototype.CNCoeff = function () {
  var config = this._config;
  var length = config.length;
  var potential = this._potential;
  var mass = config.wavefunction.mass;
  var dx = this._grid.dx;
  var dt = config.dt;
  this._matrix = {};
  this._matrix.a = new Complex(1.0,0);
  this._matrix.c = new Complex(1.0,0);
  this._matrix.b = new Array();
  var b = this._matrix.b;
  for ( var i = 0; i < length; i++ ) {
    var factor = new Complex();
    factor.re = - 2 - 2 * mass / (H_RED * H_RED) * dx * dx * potential[i];
    factor.im = 4 * mass / H_RED * dx * dx / dt;
    b.push(factor);
  }
  // pre compute cPrime
  this._matrix.cPrime = new Array();
  var cPrime = this._matrix.cPrime;
  cPrime.push(new Complex(1.0,0));
  cPrime[0].div(b[0],cPrime[0]);
  for (var i = 1; i < length - 2; i++) {
    var value = new Complex(1.0,0);
    var factor = new Complex();
    factor.add(b[i], factor);
    factor.sub(cPrime[i-1],factor);
    value.div(factor,value);
    cPrime.push(value);
  }
};
*/
/* 
 * DESIGN OPTIONS
*/
// =========================
// DESIGN LABELS CONTAINER 1
// =========================
Quantum1D.prototype.DesignXLabel = function () {
  // container 1 child 0
  var xLabel = this._plotting._scene.getChildAt(1).getChildAt(0);
  // remove children of xLabel
  xLabel.removeChildren();
  // get xLabel design
  var design = this._design.xLabel;
  if (design.plot) {
    // draw x numbers
    var colorLabel = design.color.label;
    var colorAxis = design.color.axis;
    var fontsize = design.fontsize;
    var grid = this._config.grid;
    var xMin = grid.xmin;
    var xMax = grid.xmax;
    var delta = design.delta;
    var axis = new PIXI.Graphics();
    axis.beginFill(colorAxis);
    axis.moveTo(0,this.yConvert(2*(fontsize+1)+3));
    axis.lineTo(this._plotting.width,this.yConvert(2*(fontsize+1)+3));
    axis.lineTo(this._plotting.width,this.yConvert(0));
    axis.lineTo(0,this.yConvert(0));
    axis.lineTo(0,this.yConvert(2*(fontsize+1)+3));
    axis.endFill();
    xLabel.addChild(axis);
    var axis = new PIXI.Graphics();
    axis.beginFill(0x8D6E63);
    axis.moveTo(0,this.yConvert(2*(fontsize+1)+3));
    axis.lineTo(this._plotting.width,this.yConvert(2*(fontsize+1)+3));
    axis.lineTo(this._plotting.width,this.yConvert(2*(fontsize+1)+2));
    axis.lineTo(0,this.yConvert(2*(fontsize+1)+2));
    axis.lineTo(0,this.yConvert(2*(fontsize+1)+3));
    axis.endFill();
    xLabel.addChild(axis);
    var xValue = xMin + delta;
    while (xValue < xMax) {
      var text = new PIXI.Text(Math.round(xValue), {font: fontsize+"px Roboto", fill: colorLabel});

      text.x = (xValue == (xMax + xMin)/2) ? this.xToPx(xValue) - text.width * 0.3 : this.xToPx(xValue) - text.width / 2;
      var x0 = this.xToPx(xValue);
      var x1 = this.xToPx(xValue)+1;
      
      text.y = this.yConvert(2*(fontsize+1));
      xLabel.addChild(text);
      
      // axis label vertical line
      var vertAxis = new PIXI.Graphics();
      vertAxis.beginFill(colorLabel);
      var y0 = this.yConvert(2*(fontsize+1)+1);
      var y1 = this.yConvert(2*(fontsize+1)+3);
      vertAxis.moveTo(x0,y0);
      vertAxis.lineTo(x0,y1);
      vertAxis.lineTo(x1,y1);
      vertAxis.lineTo(x1,y0);
      vertAxis.lineTo(x0,y0);
      vertAxis.endFill();
      xLabel.addChild(vertAxis);
      
      xValue += delta;
    }
    // xLabel Units
    var text = new PIXI.Text("x/(0.1nm)", {font: fontsize+"px Roboto", fill: colorLabel});
    text.x = this.xToPx(xMax) - text.width;
    text.y = this.yConvert(fontsize+2);
    xLabel.addChild(text);
    // if momentum displayed -> k units draw on left bottom
    if (this._config.momentum) {
      var length = this._config.length;
      var width = (xMax-xMin);
      var kFactor = 2 * Math.PI * (length-1) * (length-1) / (length * width * width) * 10;
      var text = new PIXI.Text("k/("+kFactor.toFixed(2)+"/nm)", {font: fontsize+"px Roboto", fill: colorLabel});
      text.x = this.xToPx(xMin)+ 1;
      text.y = this.yConvert(fontsize+2);
      xLabel.addChild(text);
    }
  }
};
// ==========================
// y (Energy Level) Labelling
// ==========================
Quantum1D.prototype.DesignYLabel = function () {
  // container 2 child 2
  var yLabel = this._plotting._scene.getChildAt(2);
  // remove children of xLabel
  // get xLabel design
  var design = this._design.yLabel;
  var config = this._config;
  config.maxEnergy = (config.maxEnergy > 0) ? config.maxEnergy : 10;
  if (design.plot) {
    var colorVert = design.color.vert;
    var maxEnergy = config.maxEnergy;
    var sampling = design.sampling;
    var height = this._plotting.height;
    var deltaEnergy = maxEnergy / (sampling);
    var physicsHeight = config.physicsHeight;
    var deltaHeight = physicsHeight * 0.618 / (sampling);  
    var fontsize = design.fontsize;
    var colorLabel = design.color.label;
    var plotHeight = height - physicsHeight + deltaHeight;
    var energy = deltaEnergy;
    var x0 = 0;
    var x1 = 2;
    var plotOverMax = true;
    while (plotHeight < physicsHeight && plotOverMax) {
      var text = new PIXI.Text(Math.round(energy*10)/10, {font: fontsize+"px Roboto", fill: colorLabel});
      text.x = x1 + 2;
      text.y = this.yConvert(plotHeight + text.height / 2);
      yLabel.addChild(text);
      // verts for energy labels
      var vertAxis = new PIXI.Graphics();
      vertAxis.beginFill(colorVert);
      var y0 = this.yConvert(plotHeight - 1);
      var y1 = this.yConvert(plotHeight);
      vertAxis.moveTo(x0,y0);
      vertAxis.lineTo(x0,y1);
      vertAxis.lineTo(x1,y1);
      vertAxis.lineTo(x1,y0);
      vertAxis.lineTo(x0,y0);
      vertAxis.endFill();
      yLabel.addChildAt(vertAxis, 2);
      plotHeight += deltaHeight;
      energy += deltaEnergy;
      if (energy > maxEnergy*1.5)
        plotOverMax = false;
    }
    // energy units
    var text = new PIXI.Text("E/eV", {font: fontsize+"px Roboto", fill: colorLabel});
    text.x = x1 + 2;
    text.y = this.yConvert(height - text.height/2);
    yLabel.addChild(text);   
  }
};
/* 
 * DIFFERENT METHODS
 * 1. Operator Splitting: exp(A+B) -> exp(A)exp(B)
 * 2. Zero Bounded Crank Nicolson
 * 3. Periodic Bounded Crank Nicolson
*/
// =========================
// Operator Splitting Method
// =========================
Quantum1D.prototype.OperatorSplitting = function () {
  this.Camera = function () {
    this.CameraView();
  };
  // InitialPhysics - Potential and Wavefunction
  this.InitialPhysics = function () {
    // init potential and wavefunction
    this.InitialStatePhysics(true);
    // pre compute expV & expT values
    this.expTPValues();
  };

  this.BuildUp = function () {
    this.InitialContainers();
    //
    this.DesignXLabel();
    // define/plot initial potential and wavefunction
    this.PhysicsPlot();
    //
    this.DesignYLabel();
  };

  this.Simulation = function () {
    for ( var i = 0; i < 3; i++){ 
      this.VPropagator();
      FFT(this._wavepacket,false);
      this.TPropagator();
      FFT(this._wavepacket,true);
      this.VPropagator();
      // update time
      this._plotting._time += this._config.dt / 10;
    }
    //this.Error(this._wavepacket, false);
    this.Plot();
  };

  return this;
};
/*
// ===========================
// Zero Bounded Crank Nicolson
// ===========================
Quantum1D.prototype.ZeroCN = function () {
  this.Camera = function () {
    this.CameraView();
  };
  this.InitialPhysics = function () {
    // init potential and wavefunction
    this.InitialStatePhysics(false);
    // calclulate a_j, 1<=j<=J-1
    var config = this._config;
    var length = config.length;
    var mass = config.wavefunction.mass;
    var dt = config.dt;
    var dx = this._grid.dx;
    var potential = this._potential;
    var a = new Array();
    a.push(new Complex());
    var value1 = new Complex();
    value1.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[1];
    value1.im = - 2 * 2 * mass / H_RED * dx * dx / dt;
    a.push(value1);
    for ( var i = 2; i < length - 1; i++) {
      var value = new Complex();
      value.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[i]; 
      value.im = - 2 * 2 * mass / H_RED * dx * dx / dt;
      var one = new Complex(1.0,0);
      one.div(a[i-1],one);
      value.sub(one,value);
      a.push(value);
    }
    this._a = a;
    // calculate omega^n_j
    var wave = this._wavepacket;
    var omega = new Array();
    omega.push(new Complex()); // omega^n_0 = 0.0 default
    for ( var i = 1; i < length - 1; i++) {
      var value = new Complex();
      var factor = new Complex();
      factor.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[i];
      factor.im = 2 * 2 * mass / H_RED * dx * dx / dt;
      value.sub(wave[i+1],value);
      factor.mul(wave[i],factor);
      value.add(factor,value);
      value.sub(wave[i-1],value);
      omega.push(value);
    }
    this._omega = omega;
    // calculate b^n_j
    var b = new Array();
    b.push(new Complex()); // b_0 = 0.0 default
    b.push(omega[1]); // b_1 = omega_1
    for ( var i = 2; i < length - 1; i++) {
      var value = new Complex();
      value.add(b[i-1],value);
      value.div(a[i-1],value);
      value.add(omega[i],value);
      b.push(value);
    }
    this._b = b;
  };
  this.BuildUp = function () {
    this.InitialContainers();
    //
    this.DesignXLabel();
    // define/plot initial potential and wavefunction
    this.PhysicsPlot();
    //
    this.DesignYLabel();
    // draw infinit potential borders

  };
  this.Simulation = function () {
    for ( var l = 0; l < 3; l++) {
      var config = this._config;
      var length = config.length;
      var wave = this._wavepacket;
      var b = this._b;
      var a = this._a;
      for ( var i = length - 2; i >= 0; i--) {
        var value = new Complex();
        value.add(wave[i+1],value);
        value.sub(b[i],value);
        value.div(a[i],value);
        wave[i] = value;
      }
      // calculate new omega and b
      var omega = this._omega;
      var mass = config.wavefunction.mass;
      var dx = this._grid.dx;
      var dt = config.dt;
      var potential = this._potential;
      for ( var i = 1; i < length - 1; i++) {
        var value = new Complex();
        var factor = new Complex();
        factor.re = 2 + 2 * mass / (H_RED * H_RED) * dx * dx * potential[i];
        factor.im = 2 * 2 * mass / H_RED * dx * dx / dt;
        value.sub(wave[i+1],value);
        factor.mul(wave[i],factor);
        value.add(factor,value);
        value.sub(wave[i-1],value);
        omega[i] = value;
      }
      // calculate b^n_j
      var b = this._b;
      for ( var i = 2; i < length - 1; i++) {
        var value = new Complex();
        value.add(b[i-1],value);
        value.div(a[i-1],value);
        value.add(omega[i],value);
        b[i] = value;
      }
      // update time
      this._plotting._time += this._config.dt / 10;
    }
    this.Plot();
  };
  return this;
};
// ===============================
// Periodic Bounded Crank Nicolson
// ===============================
Quantum1D.prototype.PeriodicNC = function () {
  this.Camera = function () {
    this.CameraView();
  };
  this.InitialPhysics = function () {
    // init potential and wavefunction
    this.InitialStatePhysics(false);
    // matrix periodic in a,b,c,cPrime
    this.CNCoeff();
  };
  this.BuildUp = function () {
    this.InitialContainers();
    //
    this.DesignXLabel();
    // define/plot initial potential and wavefunction
    this.PhysicsPlot();
    //
    this.DesignYLabel();
  };
  this.Simulation = function () {
    for ( var i = 0; i < 3; i++) {
      this.ThomasAlgorithm();
      // update time
      this._plotting._time += this._config.dt / 10;
    }
    this.Plot();
    this.Error(this._wavepacket, true);
  };
  return this;
};
*/

// ===================
// Calculate Error
// ===================
/*
Quantum1D.prototype.Error = function (wavepacket, cn) {
  var self = this;
  // wavepacket config
  var wave = this._config.wavefunction;
  var sigma = wave.sigma;
  var x0 = wave.x0;
  var mass = wave.mass;
  var energy = wave.energy;
  var k0 = Math.sqrt(energy*2*mass/(H_RED*H_RED)-1/(2*sigma*sigma));
  var t = this._plotting._time * 10;
  // constants coherent states
  var alpha = 10;
  var delta = Math.PI/2;
  var omega = self._config.potential[0].w;
  // analytic solutions
  function FreeWavePacket (x) {
    var value = new Complex(0,k0*(x-H_RED/(2*mass)*k0*t));
    value.cexp(value);
    value.mul(new Complex(sigma,0),value);
    var alpha2 = new Complex(sigma*sigma, H_RED*t/mass);
    var b = Math.atan(alpha2.im / alpha2.re);
    var a = Math.sqrt(alpha2.re*alpha2.re+alpha2.im*alpha2.im);
    var alpha = new Complex(0,-b/2);
    alpha.cexp(alpha);
    alpha.mul(new Complex(1/Math.sqrt(a),0),alpha);
    value.mul(alpha,value);
    var val2 = new Complex(2*sigma*sigma, H_RED/mass);
    var valb = Math.atan2(val2.im / val2.re);
    var vala = Math.sqrt(val2.abs2());
    var val = new Complex(0,-valb);
    val.cexp(val);
    val.mul(new Complex(1/vala,0),val);
    var expC = new Complex(-Math.pow(x-x0-k0*H_RED*t/mass,2),0);
    expC.mul(val,expC);
    expC.cexp(expC);
    value.mul(expC,value);
    return value;
  }
  function CoherentHarmOsci (x) {
    // constants
    var sigma = Math.sqrt(H_RED/(omega*mass));
    // calculation
    var e1 = new Complex(0, - omega*t/2);
    e1.cexp(e1);
    var e2im = alpha*alpha/2*Math.sin(2*(omega*t-delta))-Math.sqrt(2)*alpha*x/sigma*Math.sin(omega*t-delta);
    var e2 = new Complex(0, e2im);
    e2.cexp(e2);
    var e3re = - 1/(2*sigma*sigma)*Math.pow(x-sigma*Math.sqrt(2)*alpha*Math.cos(omega*t-delta),2);
    var e3 = new Complex(e3re,0);
    e3.cexp(e3);
    e1.mul(e2,e1);
    e1.mul(e3,e1);
    return e1;
  }
  var grid = this._grid;
  var error = 0.0;
  var analytic, practical;
  var factor = new Complex();
  for ( var i = 0; i < self._config.length; i++) {
    if (!cn)
      factor.re = Math.pow(-1,i);
    else
      factor.re = 1;
    factor.im = 0.0;
    //analytic = FreeWavePacket(grid.x[i]);
    analytic = CoherentHarmOsci(grid.x[i]);
    analytic.mul(factor,analytic);
    practical = wavepacket[i];
    analytic.sub(practical,analytic);
    error += analytic.abs2();
  }
  error *= grid.dx;
  // /= (sigma*sqrt(pi))
  error /= sigma*Math.sqrt(Math.PI);
  //error = Math.sqrt(error);
  error /= Math.sqrt(H_RED/(mass*self._config.potential[0].w)*Math.PI);
  // calculate energy
  
  //if (cn) {
  //  for ( var i = 0; i < self._config.length; i++) {
  //    factor.re = Math.pow(-1,i);
  //    factor.im = 0.0;
  //    wavepacket[i].mul(factor,wavepacket[i]);
  //  }
  //}

  if(!cn) {
    FFT(wavepacket,false);
    // expected value of kin energy
    var T = 0.0;
    var probk = 0.0;
    for ( var i = 0; i < self._config.length; i++) { 
      probk += wavepacket[i].abs2();
      T += Math.pow(this._grid.k[i],2) * wavepacket[i].abs2();
    }
    T /= (2*mass*probk);
    T *= H_RED*H_RED;
    FFT(wavepacket,true);
  }
  else {
    var length = self._config.length;
    var dx = this._grid.dx;
    var Hwave = new Array(length);
    var norm = 0.0;
    var T = 0.0;
    for (var i = 0; i < length; i++) {
      // - h^2/(2m) * [ psi(i+1)-2psi(i)+psi(i-1)]/dx^2
      Hwave[i] = new Complex();
      var next = (i + 1);
      var last = (i - 1);
      if ( i == 0 )
        last = length-1;
      else if ( i == length - 1)
        next = 0;
      Hwave[i].sub(wavepacket[i],Hwave[i]);
      Hwave[i].mul(new Complex(2,0),Hwave[i]);
      Hwave[i].add(wavepacket[last],Hwave[i]);
      Hwave[i].add(wavepacket[next],Hwave[i]);
      Hwave[i].mul(new Complex(-H_RED*H_RED/(2*mass*dx*dx)),Hwave[i]);
      // multiplay by complex
      var konjwave = new Complex(wavepacket[i].re,-wavepacket[i].im);
      Hwave[i].mul(konjwave,Hwave[i]);
      T += Hwave[i].re;
      // norm
      norm += wavepacket[i].abs2();
    }
    T /= norm;
  }

  var prob = 0.0
  var v = 0.0;
  var x = 0.0;
  for ( var i = 0; i < self._config.length; i++) {
    prob += wavepacket[i].abs2();
    v += this._potential[i]*wavepacket[i].abs2();
    // free wave
    x += this._grid.x[i]*wavepacket[i].abs2(); 
  } 
  v /= prob;
  x /= prob;
  //var deltaX = x - (x0 + H_RED*k0/mass*t);
  var deltaX = x - Math.sqrt(2*H_RED/(omega*mass))*alpha*Math.cos(omega*t-delta);
  var energy = T + v;
  ErrorHistory.push([t,error,energy, deltaX]);
  // print error history
  if(t>=500) {
    // stop animation
    $("#pause").trigger("click");
    // make html results
    var result = "t, error, energy, delta x<br/>";
    for (var i = 0; i < ErrorHistory.length; i++) {
      result += ErrorHistory[i][0] + " , " + ErrorHistory[i][1] + " , " + ErrorHistory[i][2] + " , " + ErrorHistory[i][3] + "<br/>";
    }
    $("body").html(result);
  }
};

var ErrorHistory = new Array();
*/