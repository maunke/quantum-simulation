function GaussianWave(x,x0, sigma, k0) {
  var gaussian = new Complex();
  gaussian.re = Math.exp(-(x-x0)*(x-x0)/(2*sigma*sigma));
  var p = new Complex(0,k0*x);
  gaussian.mul(p.cexp(p),gaussian);
  return gaussian;
}
