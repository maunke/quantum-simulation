// FFT bitreversal
var BitCopy = [];
function BitReversal (max) {
  BitCopy = [];
  var m = Math.log2(max);
  //
  function BitToNumber (bit) {
    var length = bit.length;
    var number = 0;
    for (var i = 0; i < length; i++) {
      number += bit[i] * Math.pow(2, length-1-i);
    }
    return number;
  }
  function BitReversalToNumber (bit) {
    var length = bit.length;
    var number = 0;
    for (var i = 0; i < length; i++) {
      number += bit[i] * Math.pow(2, i);
    }
    return number;
  }

  function ComputeBitCombinations (Bits, actBit) {
    if (actBit < 0) {
      var number = BitToNumber(Bits);
      var numberRev = BitReversalToNumber(Bits);
      if ( number != numberRev && number < numberRev) {
        BitCopy.push([number, numberRev]);
      }
    }
    else {
      var Bits0 = Bits.slice();
      var Bits1 = Bits.slice();
      Bits0.push(0);
      Bits1.push(1);
      ComputeBitCombinations(Bits0, actBit-1);
      ComputeBitCombinations(Bits1, actBit-1);
    }
  }
  ComputeBitCombinations([],m-1);
}
function FFT(amp, inv){
        var length = amp.length;
        var m = Math.log2(length);
        var val, val2, swap, swap2, sw1, sw2;
        val2 = 1;
        var n3 = length / 2;
        var n4 = 0;
        for (var i = 0; i < BitCopy.length; i++) {
          sw1 = BitCopy[i][1];
          sw2 = BitCopy[i][0];
          swap2 = amp[sw1].re;
          swap = amp[sw1].im;
          amp[sw1].re = amp[sw2].re;
          amp[sw1].im = amp[sw2].im;
          amp[sw2].re = swap2;
          amp[sw2].im = swap;
        }
        for (var j = 1; j <= m; ++j) {
            val = val2;
            val2 *= 2;
            var swap3 = 1.0;
            var swap4 = 0.0;
            var swap5 = Math.PI / val;
            var swap6 = Math.cos(swap5);
            var swap7 = -Math.sin(swap5);
            if (!inv) {
                swap7 = - swap7;
            }
            for (val4 = 0; val4 < val; ++val4) {
                for (var k = val4; k < length; k += val2) {
                    var val5 = k + val;
                    swap2 = amp[val5].re * swap3 - amp[val5].im * swap4;
                    swap = amp[val5].re * swap4 + amp[val5].im * swap3;
                    amp[val5].re = amp[k].re - swap2;
                    amp[val5].im = amp[k].im - swap;
                    amp[k].re = amp[k].re + swap2;
                    amp[k].im = amp[k].im + swap;
                }
                swap2 = swap3 * swap6 - swap4 * swap7;
                swap = swap3 * swap7 + swap4 * swap6;
                swap3 = swap2;
                swap4 = swap;
            }
        }
        if (inv) {
            val = 0;
            while (val < length) {
                var val6 = val;
                amp[val6].re = amp[val6].re / length;
                var val7 = val++;
                amp[val7].im = amp[val7].im / length;
            }
        }
        return amp;
    }
