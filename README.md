# <img src="README/quantum_logo_grey.png" height="30"> Quantum Simulation
> Eine Implementation ist auf [www.quantum-simulation.de](http://www.quantum-simulation.de) zu erreichen

Quantum ist eine nichtrelativistische quantenmechanische Simulationsumgebung für die Propagation eines Gaußschen Wellenpaketes in beliebig einstellbaren zeitunabhängigen Potentialen. Die dazugehörige Website ist in einem responsiven Design gestaltet und dementsprechend für alle gängigen browserfähigen Endgeräten optimiert darstellbar.

## Screenshots

<img src="README/quantum.png">
<img src="README/quantum_config_window.png">

## Module

Quantum enthält folgende Komponenten:

- Fast Fourier Transformation
- Split-Operator-Verfahren
- Crank-Nicolson-Verfahren
- Messung langlebiger quasistationärer Zustände (siehe Theorie)
- Plotting

## Browser Unterstützung

- Chrome
- Edge
- Firefox
- Opera
- Internet Explorer

## Download

Repositorium klonen:
```bash
git clone https://gitlab.com/maunke/quantum-simulation.git
```
## Theorie

Quantum ist im Rahmen einer ![Bachelorarbeit](src/bachelorarbeit_markusunkel.pdf) in Physik bei Herrn PD. Dr. Bernard Metsch am Helmholtz-Institut für Strahlen- und Kernphysik der Rheinischen Friedrich-Wilhelms Universität Bonn im Februar 2016 entstanden.



## Verwendete Lizenzen von Dritten

- Material Design Lite (Apache License 2.0)
- Pixi.js - 2D JavaScript Renderer (MIT License)
- JavaScript Framework jQuery (MIT License)
- MathJax (Apache License 2.0)
- jQuery.scrollTo (MIT License)
- jquery.nicescroll, Version 3.6.6 (MIT License)

## Lizenz

© Markus Unkel, 2016. Quantum ist lizensiert unter der MIT Lizenz.
